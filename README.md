# README #

This README would normally document whatever steps are necessary to get your application up and running.

# template_framework

### What is this repository for? ###

* Quick summary
	- This is a sample framework to test Web UI, Mobile, DB, API, using Java, Gradle, TestNG and Allure Report for reporting.
* Version
	- v.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
* To generate html Allure Reports:
	- $ allure serve ./build/allure-results/
	- Run it in the terminal after running the code. Do it from the project root. (e.g. $ cd  test_framework, then $ allure serve ./build/allure-results/)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: 
	- Lionel Mangoua
	- bobymangoua@gmail.com
* Other community or team contact