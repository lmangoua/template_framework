package main.java.api;

/**
 * @author: lmangoua
 * date: 06/06/20
 */

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static main.java.utils.DriverFactory.*;

public class CustomHeaders {

    //Creating a HashMap
    public static Map<String, String> customHeadersMap = new HashMap<>();
    public static String pContentTypeHeader;

    //region <This method allows you to add header to headersMaps>
    public static void buildCustomHeaders(String headerKey, String headerValue) {

        //Adding key-value pairs to a HashMap
        customHeadersMap.put(headerKey, headerValue);
    }

    //This method allows you to add header to headersMaps
    public static Map buildCustomHeader(String headerKey, String headerValue) {

        //Adding key-value pairs to a HashMap
        customHeadersMap.put(headerKey, headerValue);

        return customHeadersMap;
    }
    //endregion

    //region <This method allows you to remove a header to headersMaps >
    public static void removeCustomHeaders(String headerKey, String headerValue) {

        //Removing key-value pairs from a HashMap
        customHeadersMap.remove(headerKey, headerValue);
    }

    public static Map removeCustomHeaders1(String headerKey, String headerValue) {

        //Removing key-value pairs from a HashMap
        customHeadersMap.remove(headerKey, headerValue);

        return customHeadersMap;
    }

    public static void removeAllCustomHeaders(Map header) {

        //Removing all headers from a HashMap
        customHeadersMap.remove(header);
    }
    //endregion

    //region <Hard code header could be done in this method using Map>
    public static void buildCustomHeaders() {

        // Adding key-value pairs to a HashMap
        customHeadersMap.put("authorization", "Basic bXJkMjp6K01RYWNeP1RyTSZFKH52RzkyVV95"); //example of authorization
        customHeadersMap.put("postman-token", postmanToken);
        customHeadersMap.put("content-type", contentTypeJson);
        customHeadersMap.put("cache-control", cacheControl);
    }

    public static Map buildCustomHeaders1() {

        // Adding key-value pairs to a HashMap
        customHeadersMap.put("authorization", "Basic bXJkMjp6K01RYWNeP1RyTSZFKH52RzkyVV95"); //example of authorization
        customHeadersMap.put("postman-token", postmanToken);
        customHeadersMap.put("content-type", contentTypeJson);
        customHeadersMap.put("cache-control", cacheControl);

        return customHeadersMap;
    }
    //endregion

    //region <Hard code header could be done in this method using RequestSpecification>
    public static RequestSpecification requestHeaders_Tal() {

        RequestSpecification headers;

        builder = new RequestSpecBuilder();

        builder.addHeader("authorization", "Basic bXJkMjp6K01RYWNeP1RyTSZFKH52RzkyVV95"); //example of authorization
        builder.addHeader("postman-token", postmanToken);
        builder.addHeader("content-type", contentTypeJson);
        builder.addHeader("cache-control", cacheControl);
        respec.expectHeader("content-type", contentTypeJson);

        headers = builder.build();

        return headers;
    }

    //This method allows you add header to requestHeaders
    public static RequestSpecification requestHeaders_Tal(String headerKey, String headerValue) {

        RequestSpecification headers;
        builder = new RequestSpecBuilder();

        builder.addHeader(headerKey, headerValue);

        headers = builder.build();

        return headers;
    }
    //endregion

    //region <Get one single header>
    public static String getContentTypeHeader(String singleHeader) {

        pContentTypeHeader = customHeadersMap.get(singleHeader);

        return pContentTypeHeader;
    }
    //endregion

    //region <Get all headers>
    public static Map getAllHeader() {

        return customHeadersMap;
    }
    //endregion
}
