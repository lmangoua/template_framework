package main.java;

/**
 * @author: lmangoua
 * date: 06/06/20
 */

import main.java.utils.DriverFactory;

public class GlobalEnums extends DriverFactory {

    //region <MOBILE>
    public enum Devices {

        NEXUS_6_EMULATOR("Nexus 6 API 23", "6.0", "emulator-5554");

        public final String deviceName;
        public final String platformVersion;
        public final String udid;

        Devices(String deviceName, String platformVersion, String udid) {
            this.deviceName = deviceName;
            this.platformVersion = platformVersion;
            this.udid = udid;
        }
    }

    public enum AppInfo {

        BRANCH_APP("AppName", "PackageName");

        public final String appName;
        public final String packageName;

        AppInfo(String appName, String packageName) {
            this.appName = appName;
            this.packageName = packageName;
        }
    }
    //endregion
}
