package main.java.utils;

/**
 * @author: lmangoua
 * date: 06/06/20
 */

import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseUtility extends DriverFactory {

    public Connection connection = null;
    public ResultSet resultSet = null;
    public static Statement statement;
    public static String valueSelectMethod;

    //region <closeResultSet>
    public static void closeResultSet(ResultSet resultSet) {

        try {
            if(resultSet != null) {
                resultSet.close();
            }
        }
        catch(Exception ex) {
            log("Failed to close resultSet: --- " + ex, "ERROR", "text");
        }
    }
    //endregion

    //region <closeStatement>
    public static void closeStatement(Statement stmt) {

        try {
            if(stmt != null) {
                stmt.close();
            }
        }
        catch(Exception ex) {
            log("Failed to close Statement: --- " + ex, "ERROR", "text");
        }
    }
    //endregion

    //region <closeConnection>
    public static void closeConnection(Connection connection) {

        try {
            if(connection != null) {
                connection.close();
            }
        }
        catch(Exception ex) {
            log("Failed to close Connection: --- " + ex, "ERROR", "text");
        }
    }
    //endregion

    //region <databaseConnection>
    public Connection databaseConnection(String connectionString, String dbUser, String dbPassword) {

        try {
            Class.forName("org.postgresql.Driver").newInstance();
            Connection con = DriverManager.getConnection(connectionString, dbUser, dbPassword);
            log("Connection to database: " + connectionString + "\n Connexion FOUND!!", "INFO", "text");
            connection = con;
            return con;
        }
        catch(Exception ex) {
            log("Failed to Connect to the database: " + connectionString + "\n Connexion NOT FOUND!! --- " + ex, "ERROR", "text");
            return null;
        }
    }
    //endregion

    //region <readDataFromDatabase>
    public String[][] readDataFromDatabase(String sql, String db) {

        String[][] data = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);

            resultSet.last();
            int rowNumb = resultSet.getRow();

            ResultSetMetaData rsmd = resultSet.getMetaData();
            int columnS = rsmd.getColumnCount();

            resultSet.beforeFirst();
            data = new String[rowNumb][columnS];

            int i = 0;
            while (resultSet.next() && i < rowNumb && rowNumb < 100) {
                for (int j = 0; j < columnS; j++) {
                    data[i][j] = resultSet.getString(j + 1);
                }
                i++;
            }

            log("Successfully read data from database with: " + sql, "INFO", "text");
            return data;
        }
        catch (Exception ex) {
            log("Failed to read data from database with: '" + sql + "': --- " + ex, "ERROR", "text");
            return data;
        }
    }
    //endregion

    //region <updateReportLink>
    /**
     * This method is to UPDATE
     * Reportportal link
     */
    public boolean updateReportLink(Connection connection, String sqlQuery) {

        try(Connection conn = connection) {
            log("Query: \n\n" + sqlQuery + "\n", "INFO", "text");
            try(Statement st = conn.createStatement()) {
                conn.setAutoCommit(false);
                st.executeUpdate(sqlQuery);

                conn.commit();
                closeStatement(st);

                closeConnection(conn);
                log("Successfully updated report portal link", "INFO", "text");
                return true;
            }
            catch(SQLException ex) {
                log("Something went wrong updating the link in the database: --- " + ex.getMessage(), "ERROR", "text");
                return false;
            }
        }
        catch(SQLException ex) {
            log("Something went wrong connecting to the database: --- " + ex.getMessage(), "ERROR", "text");
            return false;
        }
    }
    //endregion

    //Generic Query Methods
    //region <deleteMethod>
    /**
     * This method is a generic DELETE query
     */
    public static void deleteMethod(Connection connection, String sqlQuery) {

        try {
            log("QUERY: \n\n" + sqlQuery + "\n", "INFO", "text");
            Statement sqlStatement = connection.createStatement();

            //execute Query
            sqlStatement.execute(sqlQuery);
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to DELETE in the database: --- " + e.getMessage());
        }
    }
    //endregion

    //region <insertMethod>
    /**
     * This method is a generic INSERT query
     */
    public static void insertMethod(Connection connection, String sqlQuery) {

        try {
            log("QUERY: \n\n" + sqlQuery + "\n", "INFO", "text");
            Statement sqlStatement = connection.createStatement();

            //execute Query
            sqlStatement.executeUpdate(sqlQuery);

            log("Successfully Inserted in the Database", "INFO", "text");
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to INSERT in the database: --- " + e.getMessage());
        }
    }
    //endregion

    //region <selectMethod>
    /**
     * This method is a generic SELECT query
     * used to get a Single value(in 1 column) from the DB
     */
    public static void selectMethod(Connection connection, String sqlQuery) {

        try {
            log("QUERY: \n\n" + sqlQuery + "\n", "INFO", "text");
            Statement sqlStatement = connection.createStatement();

            //get the contents of table from DB
            ResultSet resSet = sqlStatement.executeQuery(sqlQuery);

            while(resSet.next()) {
                valueSelectMethod = resSet.getString(1);
                log("Value Selected: '" + valueSelectMethod + "'\n", "INFO", "text");
            }

            //execute the sqlStatement
            log("Successfully Selected from the Database", "INFO", "text");
        }
        catch(Exception e) {
            log("Failed to SELECT in the database", "INFO", "text");
            Assert.fail("\n[ERROR] Failed to SELECT in the database: --- " + e.getMessage());
        }
    }
    //endregion

    //region <select_Method>
    /**
     * This method is a generic SELECT query
     * used to get a Single value(in 1 column) from the DB
     */
    public static void select_Method(Connection connection, String sqlQuery) {

        try {
            log("QUERY: \n\n" + sqlQuery + "\n", "INFO", "text");
            Statement sqlStatement = connection.createStatement();

            //get the contents of table from DB
            ResultSet resSet = sqlStatement.executeQuery(sqlQuery);

            boolean ans = resSet.next();

            log("  Key        |", "INFO", "text");
            log("--------------", "INFO", "text");

            if(ans == false) { //If the ResultSet is Empty
                log("  " + valueSelectMethod + " |\n", "INFO", "text");
                log("[ResultSet] in empty \n", "INFO", "text");
            }
            else {
                do {
                    valueSelectMethod = resSet.getString(1);
                    //Another option below
//                    valueSelectMethod = resSet.getString("Key"); //"Key" is the keyName, valueSelectMethod = value of keyName we want to extract

                    log("  " + valueSelectMethod + " |\n", "INFO", "text");
                } while(resSet.next());
            }

            //execute the sqlStatement
            log("Successfully Selected from the Database", "INFO", "text");
        }
        catch(Exception e) {
            log("Failed to SELECT in the database", "INFO", "text");
            Assert.fail("\n[ERROR] Failed to SELECT in the database: --- " + e.getMessage());
        }
    }
    //endregion

    //region <selectTableData>
    /**
     * This method is a SELECT query for multiple values and returns the table data in an ArrayList of HashMaps
     * Each HashMap is a record returned and each key-value pair is a column-value pair in the DB
     * Can be used for fetching either a single column or multi-column data, with sing or multiple records
     */
    public
    ArrayList<HashMap<String, String>> selectTableData(Connection connection, String sqlQuery) {

        try {
            log("QUERY : \n\n" + sqlQuery + "\n", "INFO", "text");
            Statement sqlStatement = connection.createStatement();

            //get the contents of table from DB
            ResultSet resSet = sqlStatement.executeQuery(sqlQuery);

            ResultSetMetaData metaData = resSet.getMetaData();
            int numOfColumns = metaData.getColumnCount();

            ArrayList<String> columns = new ArrayList<>();
            ArrayList<HashMap<String, String>> results = new ArrayList<>();

            //getting and storing all column names
            for(int i = 1; i <= numOfColumns; i++) {
                columns.add(metaData.getColumnName(i));
            }

            //add each record as a HashMap in an ArrayList
            while(resSet.next()) {
                HashMap<String, String> map = new HashMap<>();

                for(int j = 1; j <= numOfColumns; j++) {
                    map.put(columns.get(j - 1), resSet.getString(j));
                }

                results.add(map);
            }

            log("Query Results:\n\n" + results.toString(), "INFO", "text");
            return results;
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to SELECT in the database: --- " + e.getMessage());
            return null;
        }
    }
    //endregion

    //region <updateMethod>
    /**
     * This method is a generic UPDATE query
     * used to update the dB
     */
    public boolean updateMethod(Connection connection, String sqlQuery) {

        try(Connection conn = connection) {
            try(Statement st = conn.createStatement()) {
                log("Query: \n\n" + sqlQuery + "\n", "INFO", "text");
                conn.setAutoCommit(false);
                st.executeUpdate(sqlQuery);

                conn.commit();
                closeStatement(st);

                closeConnection(conn);
                log("Successfully Updated the Database", "INFO", "text");
                return true;
            }
            catch(SQLException e) {
                log("Something went wrong updating the database: --- " + e.getMessage(), "ERROR", "text");
                return false;
            }
        }
        catch(SQLException e) {
            log("Something went wrong connecting to the database: --- " + e.getMessage(), "ERROR", "text");
            return false;
        }
    }
    //endregion
}
