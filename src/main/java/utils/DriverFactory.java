package main.java.utils;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import main.java.GlobalEnums;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    //Web
    public static RemoteWebDriver driver; //For web
    public static String Execution_Web = System.getProperty("EXECUTION_WEB");
    public static WebDriverWait wait = null;
    public static final int waitTime = 60;
    public static String BROWSER = System.getProperty("BROWSER");
    public static String PLATFORM = System.getProperty("PLATFORM");
    public DesiredCapabilities capabilities;
    public ChromeOptions options;
    public String testName;
    public static final Logger LOGGER = LoggerFactory.getLogger(DriverFactory.class);
    public static PropertyFileReader property = new PropertyFileReader();
    public static String fileName = "Home";
    public static String url = property.returnPropVal(fileName, "homePageUrl");
    //Mobile
    public static AndroidDriver Driver; //For appium
    public static String Execution_Mobile = System.getProperty("EXECUTION_MOBILE");
    public static String appAbsolutePath = "";
    public static String DEVICE = System.getProperty("DEVICE");
    public static String HOST = System.getProperty("HOST");
    public static String OperatingSystem = System.getProperty("OS");
    public static GlobalEnums.Devices deviceType;
    //Headers
    public static ValidatableResponse response = null;
    public static Response res = null;
    public static RequestSpecBuilder builder;
    public static RequestSpecification requestSpec; //used with ValidatableResponse
    public static FilterableRequestSpecification requestSpecification; //used with Response
    public static ResponseSpecBuilder respec;
    public static ResponseSpecification responseSpec; //used with ValidatableResponse
    public static FilterableResponseSpecification responseSpecification; //used with Response
    public static String postmanToken = "d49b2126-380e-5d97-9daa-6486d8e5f4ac";
    public static String contentTypeJson = "application/json";
    public static String contentTypeXml = "application/xml";
    public static String contentTypeUrlEncoded = "application/x-www-form-urlencoded";
    public static String cacheControl = "no-cache";

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite()
    //region <setupWebDriver>
    public void setupWebDriver() throws MalformedURLException {

        switch(Execution_Web.toLowerCase()){

            case("remote"):
//                setupRemoteDriver();
                break;
            case("local"):
            default:
                setupLocalDriver();
                break;
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, waitTime);
    }
    //endregion

    //Web Setup
    //region <setupLocalDriver>
    private void setupLocalDriver() throws MalformedURLException {

        switch(BROWSER) {

            case("Firefox"):
                LOGGER.info("\nWeb test is Starting ... \n");

                String geckoPath = "src/main/resources/geckodriver";
                String geckoAbsolutePath;
                File geckoFile = new File(geckoPath);
                System.out.println("Gecko driver directory - " + geckoFile.getAbsolutePath());
                geckoAbsolutePath = geckoFile.getAbsolutePath();

                System.setProperty("webdriver.gecko.driver", geckoAbsolutePath + "");

                FirefoxOptions firefoxOptions = new FirefoxOptions();

                driver = new FirefoxDriver(firefoxOptions);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
            case("internetexplorer"):
                LOGGER.info("\nWeb test is Starting ... \n");

                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability("name", testName);
                capabilities.setCapability("command-timeout", "300");
                capabilities.setCapability("idle-timeout", "120");
                capabilities.setBrowserName("iexplorer");
                capabilities.setCapability("platform", PLATFORM);

                driver = new InternetExplorerDriver(capabilities);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
            case("Chrome"):
                LOGGER.info("\nWeb test is Starting ... \n");

                String chromePath = "src/main/resources/chromedriver";
                String chromeAbsolutePath;
                File chromeFile = new File(chromePath);
                System.out.println("Chrome driver directory - " + chromeFile.getAbsolutePath());
                chromeAbsolutePath = chromeFile.getAbsolutePath();

                System.setProperty("webdriver.chrome.driver", chromeAbsolutePath + "");

                options = new ChromeOptions();

                options.addArguments("--disable-extensions");
                options.addArguments("disable-infobars");
                options.addArguments("test-type");
                options.addArguments("enable-strict-powerful-feature-restrictions");
                options.setCapability(ChromeOptions.CAPABILITY, options);
                options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

                driver = new ChromeDriver(options);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
        }
    }
    //endregion

    //Mobile Setup
    //region <openMobileApp>
    public void openMobileApp(String appName) throws Exception {

        switch(Execution_Mobile.toLowerCase()) {
            case("remote"):
//                createRemoteDriver(appName);
                break;
            case("local"):
            default:
                createLocalDriver(appName);
                break;
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(Driver, waitTime);
    }
    //endregion

    //region <createLocalDriver>
    public void createLocalDriver(String appName) {

        try {
            log("Mobile test is Starting ... \n", "INFO", "text");

            File appFile = new File(appName);

            log("Local App driver directory - " + appFile.getAbsolutePath(), "INFO", "text");
            appAbsolutePath = appFile.getAbsolutePath();

            capabilities = new DesiredCapabilities();

            capabilities.setCapability("name", testName);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
            capabilities.setCapability(MobileCapabilityType.APP, appAbsolutePath + "");
            if(DEVICE.equalsIgnoreCase("emulator")) {
                log("Device is: '" + DEVICE +"'", "INFO", "text");
                deviceType = GlobalEnums.Devices.NEXUS_6_EMULATOR; //Set device type
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Android Studio emulator
                capabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard
            }
            else if(DEVICE.equalsIgnoreCase("driver palm")) {
                log("Device is: '" + DEVICE +"'", "INFO", "text");
                deviceType = GlobalEnums.Devices.NEXUS_6_EMULATOR; //Set device type
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Driver Palm Device emulator  other_device = 53edbe9b or 255b5a02
                capabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard
            }
            capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.9.1");
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false); //To reset app before each session
            capabilities.setCapability("newCommandTimeout", 15); //To make test cases fail fast in order to quickly get an error message
            capabilities.setCapability("autoAcceptAlerts", true); //To accept popup alerts
            capabilities.setCapability("autoGrantPermissions", true); //To accept popup alerts
            capabilities.setCapability(MobileCapabilityType.SUPPORTS_ALERTS, true); //To accept popup alerts
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 700); //For the App to go to sleep after 700s
            capabilities.setCapability("ignoreUnimportantViews", true); //helps to reduce the time of inputs on Android
            capabilities.setCapability("disableAndroidWatchers", true); //helps to reduce the time of inputs on Android

            log("Driver = " + HOST + ":4723/wd/hub \n", "INFO", "text"); //Appium default port 0.0.0.0:4723

            Driver = new AndroidDriver(new URL("http://" + HOST + ":4723/wd/hub"), capabilities);
            log(OperatingSystem + " on local machine initiated \n", "INFO", "text"); //Setup Android Virtual Device's name
        }
        catch (Exception e) {
            log("Failed to setup Local Mobile Driver --- " + e.getMessage(), "ERROR", "text");
        }
    }
    //endregion

    //region <Logger>
    public static void logger(final String message, final String level, String format) {

        if(format.equalsIgnoreCase(("json"))) {
            String json = (new JSONObject(message)).toString(4); //To convert into pretty Json format
            LOGGER.info("\n" + json); //To print on the console
//            ReportPortal.emitLog(json, level, Calendar.getInstance().getTime()); //To print on reportportal //TODO to fix
        }
        else {
            LOGGER.info(message); //To print on the console
//            ReportPortal.emitLog(message, level, Calendar.getInstance().getTime()); //To print on reportportal //TODO to fix
        }
    }

    public static void log(final String message, final String level, String format) {

        try {
            logger(message, level, format);
        }
        catch (JSONException err) {
            logger(message, level, "text");
        }
    }

    public static void logFile(final String message, final String level, final File file) {
        try {
            LOGGER.info(message);
//            ReportPortal.emitLog(message, level, Calendar.getInstance().getTime(), file); //TODO to fix

        }
        catch (Exception e) {
            logger(message, level, "text");
        }

    }
    //endregion

    @AfterSuite(alwaysRun = true)
    //region <tearDown>
    public void tearDown() throws IOException {

        switch (Execution_Web.toLowerCase()) {

            case ("remote"):
                boolean hasQuit = driver.toString().contains("(null)");
                if (driver != null || hasQuit == false) {
                    LOGGER.info("Test is Ending ...\n");

                    driver.quit();
                }

                break;
            default:
                if (driver != null) {
                    LOGGER.info("Test is Ending ...\n");

                    driver.quit();
                }

                break;
        }
    }
    //endregion
}
