package main.java.utils;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader extends DriverFactory {

    //Default constructor
    public PropertyFileReader() {
    }

    public String returnPropVal(String fileName, final String key){

        // get a new properties object:
        final Properties properties = new Properties();
        String value = null;

        try {
            properties.load(new FileInputStream("src/main/resources/" + fileName + ".properties"));
            // get property value based on key:
            value = properties.getProperty(key);
        }
        catch (final FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return value;
    }
}
