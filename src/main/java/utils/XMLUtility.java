package main.java.utils;

/**
 * @author: lmangoua
 * date: 06/06/20
 */

import com.google.gson.*;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class XMLUtility extends DriverFactory {

    public static Document doc = null;

    //region<load xml file from directory>
    public static Document loadXML(String filePath) {

        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        try {
            dBuilder = dbFactory.newDocumentBuilder();

            // parse xml file and load into document
            doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();
            return doc;

        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
            return doc;
        }
    }
    //endregion

    //region<Update xml value>
    public static Document updateXmlElementValue(Document doc, String nodeKey, String nodeVale) {

        Node node = doc.getElementsByTagName(nodeKey).item(0).getFirstChild();
        node.setNodeValue(nodeVale);
        return doc;
    }
    //endregion

    //region<Update xml value in a list>
    public static Document updateXmlElementValue(Document doc, String parentKey, String nodeKey, String nodeVale) {

        NodeList nodeList = doc.getElementsByTagName(parentKey);
        Element element;

        for (int i = 0; i < nodeList.getLength(); i++) {
            element = (Element) nodeList.item(i);
            Node node = element.getElementsByTagName(nodeKey).item(0).getFirstChild();
            node.setNodeValue(nodeVale);
        }

        return doc;
    }
    //endregion

    //region<method to convert Document to String>
    public static String convertXMLDocumentToString(Document doc) {

        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);

            return writer.toString();
        }
        catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    //endregion

    //region<read file and convert it to String>
    public static String generateStringFromResource(String path) throws IOException {

        File f = new File(path);

        if (f.exists() && !f.isDirectory()) {
            return new String(Files.readAllBytes(Paths.get(path)));
        }
        else {
            return null;
        }
    }
    //endregion

    //region<getXmlNodeValueByAttribute>
    public static String getXmlNodeValueByAttribute(String xmlString, String attributeName) {

        Document doc = convertStringToXMLDocument(xmlString);
        return readXmlElementValue(doc, attributeName);
    }
    //endregion

    //region<convertStringToXMLDocument>
    public static Document convertStringToXMLDocument(String xmlString) {

        // Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // API to obtain DOM Document instance
        DocumentBuilder builder;
        try {
            // Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            // Parse the content to Document object
            return builder.parse(new InputSource(new StringReader(xmlString)));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    //endregion

    //region<readXmlElementValue>
    public static String readXmlElementValue(Document doc, String nodeKey) {

        Node node = doc.getElementsByTagName(nodeKey).item(0).getFirstChild();
        return node.getNodeValue();
    }
    //endregion

    //region<Pretty print xml string>
    public static String getPrettyString(String xmlData, int indent) throws Exception {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", indent);

        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter stringWriter = new StringWriter();
        StreamResult xmlOutput = new StreamResult(stringWriter);

        Source xmlInput = new StreamSource(new StringReader(xmlData));
        transformer.transform(xmlInput, xmlOutput);

        return xmlOutput.getWriter().toString();
    }
    //endregion
}
