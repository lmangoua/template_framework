package web.testSuites;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import io.qameta.allure.Description;
import main.java.pageObjects.HomePage;
import main.java.utils.DriverFactory;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

public class RunTestEqualExpertsTest extends DriverFactory {

    @Features("Run Test Equal Experts - Navigate to page Url")
    @Description("Run Test Equal Experts - Navigate to page Url")
    @Stories("This test is to Navigate to page Url")
    @Test(priority = 0)
    public void navigateToPageUrl() {

        HomePage.goToUrl();

        System.out.println("\n=============== navigateToPageUrl test executed successfully ===============\n");
    }

    @Features("Enter Booking information - Type in information")
    @Description("Enter Booking information - Type in information")
    @Stories("This test is to Type in booking information")
    @Test(priority = 1)
    public void enterInfo() {

        HomePage.fillInForm();

        System.out.println("\n=============== enterInfo test executed successfully ===============\n");
    }

    @Features("Validate Booking Created then Delete it - Validate then delete entry")
    @Description("Validate Booking Created then Delete it - Validate then delete entry")
    @Stories("This test is to Validate then delete entry")
    @Test(priority = 2)
    public void validateBookingCreatedThenDeleteIt() {

        HomePage.validateBookingCreated();

        HomePage.deleteBookingCreated();

        System.out.println("\n=============== validateBookingCreatedThenDeleteIt test executed successfully ===============\n");
    }
}
